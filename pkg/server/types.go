package server

// Structured version of Claims Section, as referenced at
// https://tools.ietf.org/html/rfc7519#section-4.1
// See examples for how to use this with your own claim types
type StandardClaims struct {
	Audience  string `json:"aud,omitempty"`
	ExpiresAt int64  `json:"exp,omitempty"`
	Id        string `json:"jti,omitempty"`
	IssuedAt  int64  `json:"iat,omitempty"`
	Issuer    string `json:"iss,omitempty"`
	NotBefore int64  `json:"nbf,omitempty"`
	Subject   string `json:"sub,omitempty"`
}

type IDTokenPayload struct {
	Name    string   `json:"name"`
	Email   string   `json:"email"`
	Groups  []string `json:"http://smartfiscal.info/groups"`
	JsonRaw []byte
	StandardClaims
}

type KubeConfigTemplate struct {
	ClusterName   string
	ClusterServer string
	Email         string
}
