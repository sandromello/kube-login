package server

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	logLevels  = []string{"debug", "info", "error"}
	logFormats = []string{"json", "text"}
)

func decodeToken(p string) (*IDTokenPayload, error) {
	parts := strings.Split(p, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("Malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("Malformed jwt payload: %v", err)
	}
	idTokenPayload := &IDTokenPayload{JsonRaw: payload}
	if err := json.Unmarshal(payload, idTokenPayload); err != nil {
		return nil, fmt.Errorf("Failed decoding token: %v", err)
	}
	return idTokenPayload, nil
}

type Handler struct {
	cfg    *Config
	logger logrus.FieldLogger
	tmpl   *template.Template
}

func NewHandler(cfg *Config, tmpl *template.Template, logger logrus.FieldLogger) *Handler {
	return &Handler{
		cfg:    cfg,
		tmpl:   tmpl,
		logger: logger,
	}
}

func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	data := map[string]interface{}{
		"IssuerURL": template.JS(h.cfg.IssuerURL),
		"ClientID":  template.JS(h.cfg.ClientID),
	}
	h.tmpl.ExecuteTemplate(w, "login.html", data)
}

func (h *Handler) Index(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, fmt.Sprintf("Method not implemented: %s", r.Method), http.StatusMethodNotAllowed)
	}
	qs := r.URL.Query()
	rawIDToken := qs.Get("id_token")
	if rawIDToken == "" {
		h.tmpl.ExecuteTemplate(w, "index.html", nil)
		return
	}

	claims, err := decodeToken(rawIDToken)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	idTokenInfo := ""
	if qs.Get("tokenInfo") == "1" {
		buff := new(bytes.Buffer)
		json.Indent(buff, []byte(claims.JsonRaw), "", "  ")
		idTokenInfo = string(buff.Bytes())
	}
	kubeConfigURL := template.JS(fmt.Sprintf(
		"kubeconfig.yaml?email=%s&id_token=%s",
		claims.Email,
		rawIDToken,
	))
	// showPublicK8sRootCA := false
	// if h.cfg.PublicK8sAPIServerRootCA != "" {
	// 	showPublicK8sRootCA = true
	// }

	exp := time.Unix(claims.ExpiresAt, 0).UTC()
	timeToExpire := time.Since(exp).Round(time.Second) * -1
	data := map[string]interface{}{
		"title":         "Smart Fiscal - K8S Authentication",
		"IDToken":       rawIDToken,
		"IDTokenInfo":   idTokenInfo,
		"ClusterName":   h.cfg.ClusterName,
		"Email":         claims.Email,
		"Name":          claims.Name,
		"Groups":        claims.Groups,
		"KubeConfigURL": kubeConfigURL,
		"ClusterSize":   h.cfg.ClusterSize,
		"ProductDomain": h.cfg.ProductDomain,
		"ExpireAt":      exp.Format(time.RFC822Z),
		"TimeToExpire":  fmt.Sprintf("%v to expire", timeToExpire),
	}
	h.tmpl.ExecuteTemplate(w, "index.html", data)
}

// Callback exchanges a token for the received code and then redirect to index
func (h *Handler) Callback(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, fmt.Sprintf("Method not implemented: %s", r.Method), http.StatusMethodNotAllowed)
	}
	if errMsg := r.FormValue("error"); errMsg != "" {
		http.Error(w, errMsg+": "+r.FormValue("error_description"), http.StatusBadRequest)
		return
	}
	data := map[string]interface{}{
		"IssuerURL": template.JS(h.cfg.IssuerURL),
		"ClientID":  template.JS(h.cfg.ClientID),
	}
	h.tmpl.ExecuteTemplate(w, "callback.html", data)
}

func (h *Handler) K8sRootCA(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", r.Header.Get("Content-Type"))
	w.Header().Set(
		"Content-Disposition",
		fmt.Sprintf("attachment; filename=%s-ca.crt", h.cfg.ClusterName),
	)
	w.Write([]byte(h.cfg.PublicK8sAPIServerRootCA))
}

func (h *Handler) KubeConfig(w http.ResponseWriter, r *http.Request) {
	qs := r.URL.Query()
	var kubeConfigTemplates []KubeConfigTemplate
	for i := 1; i <= h.cfg.ClusterSize; i++ {
		kct := KubeConfigTemplate{
			ClusterName:   fmt.Sprintf("ke%d.%s", i, h.cfg.ProductDomain),
			ClusterServer: fmt.Sprintf("https://api.ke%d.%s", i, h.cfg.ProductDomain),
			Email:         qs.Get("email"),
		}
		if i <= 9 {
			kct.ClusterName = fmt.Sprintf("ke0%d.%s", i, h.cfg.ProductDomain)
			kct.ClusterServer = fmt.Sprintf("https://api.ke0%d.%s", i, h.cfg.ProductDomain)
		}
		kubeConfigTemplates = append(kubeConfigTemplates, kct)
	}
	data := map[string]interface{}{
		"IDToken":            qs.Get("id_token"),
		"KubeConfigClusters": kubeConfigTemplates,
		"ClusterName":        kubeConfigTemplates[0].ClusterName,
		"CurrentContext":     kubeConfigTemplates[0].ClusterName,
		"Email":              qs.Get("email"),
	}
	w.Header().Set("Content-Type", "application/x-yaml")
	w.Header().Set("Content-Disposition", "attachment; filename=kubeconfig.yaml")
	h.tmpl.ExecuteTemplate(w, "kubeconfig.yaml", data)
}

type utcFormatter struct {
	f logrus.Formatter
}

func (f *utcFormatter) Format(e *logrus.Entry) ([]byte, error) {
	e.Time = e.Time.UTC()
	return f.f.Format(e)
}

func NewLogger(level string, format string) (logrus.FieldLogger, error) {
	var logLevel logrus.Level
	switch strings.ToLower(level) {
	case "debug":
		logLevel = logrus.DebugLevel
	case "", "info":
		logLevel = logrus.InfoLevel
	case "error":
		logLevel = logrus.ErrorLevel
	default:
		return nil, fmt.Errorf("log level is not one of the supported values (%s): %s", strings.Join(logLevels, ", "), level)
	}

	var formatter utcFormatter
	switch strings.ToLower(format) {
	case "", "text":
		formatter.f = &logrus.TextFormatter{DisableColors: true}
	case "json":
		formatter.f = &logrus.JSONFormatter{}
	default:
		return nil, fmt.Errorf("log format is not one of the supported values (%s): %s", strings.Join(logFormats, ", "), format)
	}

	return &logrus.Logger{
		Out:       os.Stderr,
		Formatter: &formatter,
		Level:     logLevel,
	}, nil
}
