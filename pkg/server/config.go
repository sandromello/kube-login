package server

import (
	"io/ioutil"
	"log"
	"strings"
)

// Config defines configuration parameters for dex kubeconfig
type Config struct {
	AssetsRootDir            string
	ClientID                 string
	ClientSecret             string
	RedirectURI              string
	IssuerURL                string
	ClusterName              string
	APIServer                string
	PublicK8sAPIServer       string
	PublicK8sAPIServerRootCA string
	BindAddress              string
	Port                     int
	RootCAs                  string
	Scopes                   string
	ProductDomain            string
	ClusterSize              int
}

// GetScopes return then as a slice of strings
func (c *Config) GetScopes() []string {
	return strings.Split(c.Scopes, " ")
}

// LoadPublicK8sAPIServerRootCA load the certificate authority file into memory
func (c *Config) LoadPublicK8sAPIServerRootCA() {
	rootCABytes, err := ioutil.ReadFile(c.PublicK8sAPIServerRootCA)
	if err != nil {
		log.Fatalf("failed to read root-ca: %v", err)
	}
	c.PublicK8sAPIServerRootCA = string(rootCABytes)
}
