# Kube Auth0 Open ID Connect

Dashboard UI to download kubeconfig file and view information of Open ID Connect JWT tokens.

## Build Local

```bash
# GOOS defaults to linux
GOOS=darwin GIT_TAG=v0.0.1 make build
GIT_TAG=v0.0.1 make docker-build
DOCKER_USERNAME= DOCKER_PASSWORD= GIT_TAG=v0.0.1 make docker-push
```

# Development - update dependencies

- Install [dep](https://github.com/golang/dep)

dep ensure -update
