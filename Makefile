SHORT_NAME = kube-login

include versioning.mk

REPO_PATH := github.com/cloud104/${SHORT_NAME}

BINARY_DEST_DIR := rootfs/usr/bin
ASSETS_DEST_DIR := rootfs/app/web
GOOS ?= linux
GOARCH ?= amd64

SHELL ?= /bin/bash

# Common flags passed into Go's linker.
GOTEST := go test --race -v
LDFLAGS := "-s -w \
-X github.com/cloud104/kube-login/pkg/version.version=${VERSION} \
-X github.com/cloud104/kube-login/pkg/version.gitCommit=${GITCOMMIT} \
-X github.com/cloud104/kube-login/pkg/version.buildDate=${DATE}"

build:
	rm -rf ${BINARY_DEST_DIR} ${ASSETS_DEST_DIR}
	mkdir -p ${BINARY_DEST_DIR} ${ASSETS_DEST_DIR}
	cp -a web/* ${ASSETS_DEST_DIR}/
	env GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags ${LDFLAGS} -o ${BINARY_DEST_DIR}/kube-login cmd/main.go

docker-build:
	docker build --rm -t ${IMAGE} rootfs
	docker tag ${IMAGE} ${MUTABLE_IMAGE}

test: test-unit

test-unit:
	${GOTEST} ./pkg/...

.PHONY: build test docker-build
