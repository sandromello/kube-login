# Deploy 

- Create a secret with the client-id:

```bash
kubectl create secret generic kube-login --from-file=path/to/clientid
kubectl apply -f ./manifests
```
