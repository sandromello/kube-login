package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"

	"github.com/cloud104/kube-login/pkg/server"
	"github.com/cloud104/kube-login/pkg/version"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var cfg server.Config

func cmd() *cobra.Command {
	c := cobra.Command{
		Use:   "kube-dex",
		Short: "An OpenID Connect client for TOTVS Smart Fiscal",
		Long:  "",
		RunE: func(cmd *cobra.Command, args []string) error {
			if cfg.ClientID == "" {
				cfg.ClientID = os.Getenv("CLIENT_ID")
				if cfg.ClientID == "" {
					logrus.Fatalf("Missing required option '--client-id'")
				}
			}
			if cfg.IssuerURL == "" {
				logrus.Fatalf("Missing required option '--issuer-url'")
			}
			staticDir := path.Join("", cfg.AssetsRootDir, "web/static")
			fs := http.FileServer(http.Dir(staticDir))
			http.Handle("/static/", http.StripPrefix("/static/", fs))

			logger, err := server.NewLogger(logrus.InfoLevel.String(), "text")
			if err != nil {
				return fmt.Errorf("invalid config: %v", err)
			}

			// Load templates
			tmpl, err := template.New("").ParseFiles(
				filepath.Join("", cfg.AssetsRootDir, "web/templates", "login.html"),
				filepath.Join("", cfg.AssetsRootDir, "web/templates", "index.html"),
				filepath.Join("", cfg.AssetsRootDir, "web/templates", "callback.html"),
				filepath.Join("", cfg.AssetsRootDir, "web/templates", "kubeconfig.yaml"),
			)
			if err != nil {
				log.Fatalf("Failed parsing templates: %v", err)
			}

			handler := server.NewHandler(&cfg, tmpl, logger)
			http.HandleFunc("/", handler.Index)
			http.HandleFunc("/login", handler.Login)
			// http.HandleFunc("/ca.crt", handler.K8sRootCA)
			http.HandleFunc("/callback", handler.Callback)
			http.HandleFunc("/kubeconfig.yaml", handler.KubeConfig)

			vinfo := version.Get()
			fmt.Println("----------------------------------")
			fmt.Println("Build Date: ", vinfo.BuildDate)
			fmt.Println("Compiler: ", vinfo.Compiler)
			fmt.Println("Git Commit: ", vinfo.GitCommit)
			fmt.Println("GO Version: ", vinfo.GoVersion)
			fmt.Println("Platform: ", vinfo.Platform)
			fmt.Println("Version: ", vinfo.Version)
			fmt.Println("----------------------------------")

			listenAddr := fmt.Sprintf("%s:%d", cfg.BindAddress, cfg.Port)
			logrus.Infof("Starting kube-auth0 server at %s ...", listenAddr)
			return http.ListenAndServe(listenAddr, nil)
		},
	}
	c.Flags().StringVar(&cfg.AssetsRootDir, "assets-root-dir", "/app", "Client ID of auth0")
	c.Flags().StringVar(&cfg.ClientID, "client-id", "", "Client ID of auth0")
	c.Flags().StringVar(&cfg.IssuerURL, "issuer-url", "", "The issuer URL of auth0")
	c.Flags().StringVar(&cfg.PublicK8sAPIServer, "public-api-server", "", "The public Kubernetes API server address.")
	c.Flags().StringVar(&cfg.ProductDomain, "product-domain", "smartfiscal.info", "The public address that will be used to create kubeconfig cluster uris")
	c.Flags().IntVar(&cfg.ClusterSize, "cluster-size", 12, "The ammount of clusters that will be generated")
	c.Flags().StringVar(&cfg.BindAddress, "bind-address", "0.0.0.0", "Bind address to serve")
	c.Flags().IntVar(&cfg.Port, "port", 8080, "Port to serve")
	return &c
}

func main() {
	if err := cmd().Execute(); err != nil {
		log.Fatalf("Failed starting app: %v", err)
	}
}
